# Pour la phase 1 du projet

## Il faut exécuter la commande suivante 

python TraitementImage/CBIR/src/color.py



# Pour la phase 2 du projet

## Classer les dossiers pour la base de données d'images

python TraitementImage/CBIR/src/classsification.py

## Apprentissage

python TraitementImage/CBIR/src/classifier.py

## Prédiction

python TraitementImage/CBIR/src/prediction.py



