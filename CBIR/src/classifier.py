'''This script goes along the blog post
"Building powerful image classification models using very little data"
from blog.keras.io.
It uses data that can be downloaded at:
https://www.kaggle.com/c/dogs-vs-cats/data
In our setup, we:
- created a data/ folder
- created train/ and validation/ subfolders inside data/
- created cats/ and dogs/ subfolders inside train/ and validation/
- put the cat pictures index 0-999 in data/train/cats
- put the cat pictures index 1000-1400 in data/validation/cats
- put the dogs pictures index 12500-13499 in data/train/dogs
- put the dog pictures index 13500-13900 in data/validation/dogs
So that we have 1000 training examples for each class, and 400 validation examples for each class.
In summary, this is our directory structure:
```
data/
    train/
        dogs/
            dog001.jpg
            dog002.jpg
            ...
        cats/
            cat001.jpg
            cat002.jpg
            ...
    validation/
        dogs/
            dog001.jpg
            dog002.jpg
            ...
        cats/
            cat001.jpg
            cat002.jpg
            ...
```
'''

from keras.preprocessing.image import ImageDataGenerator
from keras.models import Sequential
from keras.layers import Conv2D, MaxPooling2D
from keras.layers import Activation, Dropout, Flatten, Dense
#from keras.utils import plot_model
from keras import backend as K
import os

def getNbSamples(dir_path):
    dir_ = os.path.abspath(os.path.pardir + dir_path)
    folders = os.listdir(dir_)
    folders_path = []
    for folder in folders:
        folders_path.append(os.path.abspath(os.path.pardir + dir_path + '/' + folder))
        
    counter = 0
    for i in range(len(folders_path)):
        files = os.listdir(folders_path[i])
        for i in range(len(files)):
            if "jpg" not in files[i]:
                del files[i]
        counter += len(files)
    return counter

def getNbClasses(dir_path):
    dir_ = os.path.abspath(os.path.pardir + dir_path)
    folders = os.listdir(dir_)
    return len(folders)

# dimensions of our images.
img_width, img_height = 150, 150

train_data_dir = os.path.abspath(os.path.pardir + '/data/train')
validation_data_dir = os.path.abspath(os.path.pardir + '/data/validation')
nb_train_samples = getNbSamples('/data/train')
nb_validation_samples = getNbSamples('/data/validation')
epochs = 50
batch_size = 16
nbclasses = getNbClasses('/data/train')

if K.image_data_format() == 'channels_first':
    input_shape = (3, img_width, img_height)
else:
    input_shape = (img_width, img_height, 3)

destination_file = 'first.h5'

if os.path.exists(destination_file):
    os.remove(destination_file)

model = Sequential()
model.add(Conv2D(32, (3, 3), input_shape=input_shape))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(32, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(64, (3, 3)))
model.add(Activation('relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Flatten()) #mettre a plat tous les coef des neurones 
model.add(Dense(64)) #mettre des liens entre toutes les couches de réseau neuronne
model.add(Activation('relu'))
model.add(Dropout(0.5))
model.add(Dense(nbclasses)) #nombre de classes a la place du 1
model.add(Activation('softmax')) #softmax, #sigmoid

model.compile(loss='categorical_crossentropy',
              optimizer='rmsprop',
              metrics=['accuracy'])

# this is the augmentation configuration we will use for training
train_datagen = ImageDataGenerator(
    rescale=1. / 255,
    shear_range=0.2,
    zoom_range=0.2,
    horizontal_flip=True)

# this is the augmentation configuration we will use for testing:
# only rescaling
test_datagen = ImageDataGenerator(rescale=1. / 255)

train_generator = train_datagen.flow_from_directory(
    train_data_dir,
    target_size=(img_width, img_height),
    batch_size=batch_size,
    class_mode='categorical')

classes = train_generator.class_indices

validation_generator = test_datagen.flow_from_directory(
    validation_data_dir,
    target_size=(img_width, img_height),
    batch_size=batch_size,
    class_mode='categorical')

history = model.fit_generator(
    train_generator,
    steps_per_epoch=nb_train_samples // batch_size,
    epochs=epochs,
    validation_data=validation_generator,
    validation_steps=nb_validation_samples // batch_size)

model.save(destination_file)
#plot_model(model, to_file='')
