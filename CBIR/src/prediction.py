# -*- coding: utf-8 -*-
"""
Created on Tue Jan 28 09:19:16 2020

@author: Alioune BA
"""
# load_model_sample.py
from keras.models import load_model
from keras.preprocessing import image
import matplotlib.pyplot as plt
import numpy as np
import os
import shutil
from shutil import rmtree

def load_image(img_path, show=False):
    img = image.load_img(img_path, target_size=(150, 150))
    img_tensor = image.img_to_array(img)                    # (height, width, channels)
    img_tensor = np.expand_dims(img_tensor, axis=0)         # (1, height, width, channels), add a dimension because the model expects this shape: (batch_size, height, width, channels)
    img_tensor /= 255.                                      # imshow expects values in the range [0, 1]

    if show:
        plt.imshow(img_tensor[0])                           
        plt.axis('off')
        plt.show()

    return img_tensor


if __name__ == '__main__' :
    
    h5_file = 'first.h5'
     # load model
    model = load_model(h5_file)
    result = '/data/result/'
    test_dir = os.path.abspath(os.path.pardir + '/data/test')

    folders = os.listdir(test_dir)
    for folder in folders:
        if os.path.exists(os.path.abspath(os.path.pardir + result + folder)):
            rmtree(os.path.abspath(os.path.pardir + result + folder))
        os.mkdir(os.path.abspath(os.path.pardir + result + folder))
    
    for folder in folders:
        folder_images = os.path.abspath(os.path.pardir + '/data/test/' + folder)
    
        images = os.listdir(folder_images)
        
        for image_ in images:
            im = os.path.abspath(os.path.pardir + '/data/test/' + folder + '/' + image_)
          
            model.compile(loss='categorical_crossentropy',
                          optimizer='rmsprop',
                          metrics=['accuracy'])

            # load a single image
            new_image = load_image(im)
        
            # check prediction
            pred = model.predict(new_image)
            classes = model.predict_classes(new_image)
            for i in range(len(folders)):
                if classes[0] == i:
                    destination = os.path.abspath(os.path.pardir + result + folders[i] + '/' + image_)
            shutil.copy(im, destination)
            
            print(pred)
            print(classes)
    