# -*- coding: utf-8 -*-
"""
Created on Tue Jan 14 07:58:33 2020

@author: Alioune BA
"""
import os
from shutil import copyfile, rmtree
import random

class Classification():
    
    def __init__(self):
        self.data_dir = os.path.pardir + "/data"
        self.source = os.path.pardir + "/../CorelDB/"
        self.train_dir = self.data_dir + "/train/"
        self.validation_dir = self.data_dir + "/validation/"
        self.test_dir = self.data_dir + "/test/"
        self.list_dir = (self.train_dir, self.validation_dir, self.test_dir)
            
    def lister_repertoires(self):
        self.folders = os.listdir(os.path.abspath(self.source))
        
    def random_choice(self):
        rand = random.choice(self.folders)
        if rand in self.folders_:
            rand = self.random_choice()
        else:
            self.folders_.append(rand)
        return rand
    
    def creation_repertoires(self, n):
        self.folders_ = []
        
        for i in range(n):
            self.random_choice()
            
                
        for dir_ in self.list_dir:
            for name in self.folders_:
                try:
                    os.mkdir(os.path.abspath(dir_ + name))
                except OSError:
                    if(os.path.isdir(dir_+name)):
                        print("Le répertoire {} existe déjà".format(dir_+name))
                    else:
                        print("La création du répertoire {} a échoué".format(dir_+name))
                else:
                    print("le répertoire {} est créé".format(dir_+name))
                    
    def nettoyer(self):
        for dir_ in self.list_dir:
            fff = os.listdir(os.path.abspath(dir_))
            for name in fff:
                if(os.path.isdir(os.path.abspath(dir_+name))):
                    rmtree(os.path.abspath(dir_+name))

    def compter_fichiers(self):
        count = 0
        for folder in self.folders_:
            count += len(os.listdir(os.path.abspath(self.source + folder)))
            
        print(count)
    
    def copier_fichiers(self):
        for folder in self.folders_:
            source = os.path.abspath(self.source + folder)
            i = 0
            j = 0
            for file in os.listdir(source):
                dest_ = folder + "/" + file
                file = os.path.abspath(self.source + dest_)
                if(i%10==0):
                    j = 0
                    print("reset")

                if(j < 5):
                    dest = os.path.abspath(self.train_dir + dest_)
                    copyfile(file, dest)
                else:
                    if(j<8):
                        dest = os.path.abspath(self.validation_dir + dest_)
                        copyfile(file, dest)
                    else:
                        dest = os.path.abspath(self.test_dir + dest_)
                        copyfile(file, dest)
                j += 1
                i += 1    


if __name__ == '__main__' :
    classif = Classification()
    classif.nettoyer()
    classif.lister_repertoires()
    classif.creation_repertoires(3)
    #0classif.compter_fichiers()
    classif.copier_fichiers()
    